import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import {
  AuthController,
  RegController,
} from "../lib/controllers/auth.controller";
import {
  checkResponseTime,
  checkStatusCode,
  checkJsonSchema,
  validateSchema,
} from "../../helpers/functionsForChecking.helper";
import Chance from "chance";
import {
  schema_allUsers,
  schema_login,
  schema_user,
  schema_post,
  schema_comment,
} from "/Users/oksanakazanchuk/Desktop/pet-api-project/pet-api-testing-03-04/tests/api/specs/data/schemas_testData.json";
import { PostsController } from "../lib/controllers/posts.controller";

const users = new UsersController();
const auth = new AuthController();
const reg = new RegController();
const posts = new PostsController();
const chance = new Chance();

let registeredEmail: string;
let registeredPassword: string;
let registeredUserAvatar: string;
let registeredUsername: string;
let registeredUserId: string;
let accessToken: string;
let loginResponse: any;
let updatedUserData: any;
let postId: number;
let body: string;

describe("End2end user flow", () => {
  before("Register and login", async () => {
    registeredEmail = chance.email();
    registeredPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUsername = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await reg.registration(
      registeredEmail,
      registeredPassword,
      registeredUsername
    );
    registeredUserId = registrationResponse.body.user.id;
    registeredUserAvatar = registrationResponse.body.user.avatar;

    loginResponse = await auth.login(registeredEmail, registeredPassword);
    accessToken = loginResponse.body.token.accessToken.token;
  });

  it("Get all users", async () => {
    const response = await users.getAllUsers();

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body.length).to.be.greaterThan(0);
  });

  it("Get all posts", async () => {
    const response = await posts.getAllPosts();
    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body).to.be.an("array").that.is.not.empty;
  });

  it("Create post", async () => {
    body = chance.string({
      length: 20,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    const newPost = {
      authorId: registeredUserId,
      previewImage: undefined,
      body: body,
    };
    const response = await posts.createPost(newPost, accessToken);
    postId = response.body.id;

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body.body).to.equal(newPost.body);
  });

  it("Like post", async () => {
    const newPostLike = {
      entityId: postId,
      isLike: true,
      userId: registeredUserId,
    };
    const response = await posts.likePost(newPostLike, accessToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);

    const allPostsResponse = await posts.getAllPosts();
    const allPosts = allPostsResponse.body;
    const createdPost = allPosts.find((post) => post.id === postId);
    const addedLike = createdPost.reactions.find(
      (like: any) => like.user.id === registeredUserId
    );

    expect(addedLike).to.exist;
  });

  after("Delete current user", async () => {
    const response = await users.deleteUser(accessToken, registeredUserId);
    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
  });
});

import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async createPost(postData : object, accessToken : string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body(postData)
            .bearerToken (accessToken)
            .send();
        return response;
    }
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async likePost(likePostData : object, accessToken : string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(likePostData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
    
    async addComment(comment : object, accessToken : string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(comment)
            .bearerToken(accessToken)
            .send();
            return response;

    }

}
import { expect } from "chai";
import { IncomingMessage } from "http";

export function checkStatusCode(
  response,
  statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500
) {
  expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(
    statusCode
  );
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
  expect(
    response.timings.phases.total,
    `Response time should be less than ${maxResponseTime}ms`
  ).to.be.lessThan(maxResponseTime);
}

export function checkJsonSchema(data: any, schema: any) {
  expect(data.user).to.deep.equal(schema.schema_user);
}

export function validateSchema(obj: any, schema: any) {
  for (const key in schema.properties) {
    expect(obj).to.have.property(key);
    const propertySchema = schema.properties[key];
    if (propertySchema.type === "object") {
      validateSchema(obj[key], propertySchema);
    } else {
      expect(obj[key]).to.be.a(propertySchema.type);
    }
  }
}
